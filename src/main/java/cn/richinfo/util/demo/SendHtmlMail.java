package cn.richinfo.util.demo;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceFileResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendHtmlMail {
	private static final Logger logger = LoggerFactory.getLogger(SendHtmlMail.class);

	public int test() {
		File html = new File("html/index.html");
		
		String htmlEmailTemplate = null;
		try {
			htmlEmailTemplate = FileUtils.readFileToString(html);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return -1;
		}

		ImageHtmlEmail email = new ImageHtmlEmail();

		try {
			email.setDataSourceResolver(new DataSourceFileResolver(html.getParentFile()));
			email.setHostName("smtp.chinamobile.com");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("sysinfo@chinamobile.com", "NzQX3Z92mMzkSqQj"));
			email.setSSLOnConnect(true);
			email.setFrom("sysinfo@chinamobile.com");
			email.addTo("edm@chinamobile.com");
			email.setCharset("UTF-8");
			email.setSubject("China Mobile Enterprise Mail");
			email.setHtmlMsg(htmlEmailTemplate);
			email.send();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1;
		} 

		return 0;
	}

	public static void main(String[] args) {
		SendHtmlMail shm = new SendHtmlMail();
		int ret = shm.test();
		if (ret >= 0) {
			logger.info("send success");
		} else {
			logger.error("send fail");
		}
	}
}
