package cn.richinfo.util.demo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.resolver.DataSourceFileResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Html2Eml {
	private static final Logger logger = LoggerFactory.getLogger(Html2Eml.class);

	public int test() {
		File eml = new File("html.eml");
		File html = new File("html/index.html");
		
		String htmlEmailTemplate = null;
		try {
			htmlEmailTemplate = FileUtils.readFileToString(html);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return -1;
		}

		ImageHtmlEmail email = new ImageHtmlEmail();
		MimeMessage message = null;

		try {
			email.setDataSourceResolver(new DataSourceFileResolver(html.getParentFile()));
			email.setHostName("smtp.chinamobile.com");
			email.setFrom("sysinfo@chinamobile.com");
			email.addTo("dem@chinamobile.com");
			email.setSubject("China Mobile Enterprise Mail");
			email.setHtmlMsg(htmlEmailTemplate);
			email.buildMimeMessage();
			message = email.getMimeMessage();
			message.writeTo(new FileOutputStream(eml));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return -1;
		}

		return 0;
	}

	public static void main(String[] args) {
		Html2Eml h2e = new Html2Eml();
		int ret = h2e.test();
		if (ret >= 0) {
			logger.info("conv success");
		} else {
			logger.error("conv fail");
		}
	}
}
